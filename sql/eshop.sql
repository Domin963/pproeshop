-- --------------------------------------------------------
-- Hostitel:                     127.0.0.1
-- Verze serveru:                10.6.5-MariaDB - mariadb.org binary distribution
-- OS serveru:                   Win64
-- HeidiSQL Verze:               11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


-- Exportování struktury databáze pro
DROP
    DATABASE IF EXISTS `eshop`;
CREATE
    DATABASE IF NOT EXISTS `eshop` /*!40100 DEFAULT CHARACTER SET utf8mb3 */;
USE `eshop`;

-- Exportování struktury pro tabulka eshop.adresses
DROP TABLE IF EXISTS `adresses`;
CREATE TABLE IF NOT EXISTS `adresses`
(
    `id`                    int(11)      NOT NULL AUTO_INCREMENT,
    `city`                  varchar(255) DEFAULT NULL,
    `postal_code`           varchar(255) NOT NULL,
    `street_name_house_num` varchar(255) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            )
) ENGINE = InnoDB
  AUTO_INCREMENT = 20
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.adresses: ~19 rows (přibližně)
/*!40000 ALTER TABLE `adresses`
    DISABLE KEYS */;
INSERT INTO `adresses` (`id`, `city`, `postal_code`, `street_name_house_num`)
VALUES (1, 'Jihlava', '58601', 'Jihlavská 12'),
       (2, 'Hradec Králové', '12345', 'Hradecká 14'),
       (3, 'Hradec Králové', '65763', 'Hradecká 12'),
       (4, 'Pardubice', '87912', 'Pardubická 78'),
       (5, 'Plzeň', '72462', 'Plzenská 32'),
       (6, 'Pelhřimov', '65223', 'První 223'),
       (7, 'Chomutov', '76872', 'Druhá 23'),
       (8, 'Uničov', '76242', 'Uliční 54'),
       (9, 'Praha', '56232', 'Tyršova 12'),
       (10, 'Jihlava', '58601', 'Jihlavská 12'),
       (11, 'Jihlava', '58601', 'Jihlavská 12'),
       (12, 'Praha', '56232', 'Tyršova 12'),
       (13, 'Praha', '56232', 'Tyršova 12'),
       (14, 'Uničov', '76242', 'Uliční 54'),
       (15, 'Uničov', '76242', 'Uliční 54'),
       (16, 'Chomutov', '76872', 'Druhá 23'),
       (17, 'Chomutov', '76872', 'Druhá 23'),
       (18, 'Pelhřimov', '65223', 'První 223'),
       (19, 'Pelhřimov', '65223', 'První 223');
/*!40000 ALTER TABLE `adresses`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.cart
DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart`
(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    PRIMARY KEY
        (
         `id`
            )
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.cart: ~9 rows (přibližně)
/*!40000 ALTER TABLE `cart`
    DISABLE KEYS */;
INSERT INTO `cart` (`id`)
VALUES (1),
       (2),
       (3),
       (4),
       (5),
       (6),
       (7),
       (8),
       (9);
/*!40000 ALTER TABLE `cart`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.cart_cart_products
DROP TABLE IF EXISTS `cart_cart_products`;
CREATE TABLE IF NOT EXISTS `cart_cart_products`
(
    `cart_id`          int(11) NOT NULL,
    `cart_products_id` int(11) NOT NULL,
    UNIQUE KEY `UK_6iyqfvapdsmcl4d7fenxqtl18`
        (
         `cart_products_id`
            ),
    KEY `FKcxat3enqpvlarcm20bkrl7b9`
        (
         `cart_id`
            ),
    CONSTRAINT `FK84skyyjnt6ygna9u3dwuyj3gc` FOREIGN KEY
        (
         `cart_products_id`
            ) REFERENCES `cart_product`
            (
             `id`
                ),
    CONSTRAINT `FKcxat3enqpvlarcm20bkrl7b9` FOREIGN KEY
        (
         `cart_id`
            ) REFERENCES `cart`
            (
             `id`
                )
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.cart_cart_products: ~0 rows (přibližně)
/*!40000 ALTER TABLE `cart_cart_products`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_cart_products`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.cart_product
DROP TABLE IF EXISTS `cart_product`;
CREATE TABLE IF NOT EXISTS `cart_product`
(
    `id`                int(11) NOT NULL AUTO_INCREMENT,
    `quantity`          int(11) NOT NULL,
    `product_detail_id` int(11) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            ),
    KEY `FK97cl3f6tw67gvf0u33hujfffy`
        (
         `product_detail_id`
            ),
    CONSTRAINT `FK97cl3f6tw67gvf0u33hujfffy` FOREIGN KEY
        (
         `product_detail_id`
            ) REFERENCES `product_details`
            (
             `id`
                )
) ENGINE = InnoDB
  AUTO_INCREMENT = 21
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.cart_product: ~0 rows (přibližně)
/*!40000 ALTER TABLE `cart_product`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_product`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            )
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.categories: ~7 rows (přibližně)
/*!40000 ALTER TABLE `categories`
    DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`)
VALUES (1, 'stoly'),
       (2, 'židle'),
       (3, 'komody'),
       (4, 'dveře'),
       (5, 'skříně'),
       (6, 'postele'),
       (7, 'okna');
/*!40000 ALTER TABLE `categories`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.materials
DROP TABLE IF EXISTS `materials`;
CREATE TABLE IF NOT EXISTS `materials`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            )
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.materials: ~7 rows (přibližně)
/*!40000 ALTER TABLE `materials`
    DISABLE KEYS */;
INSERT INTO `materials` (`id`, `name`)
VALUES (1, 'buk'),
       (2, 'bříza'),
       (3, 'kámen'),
       (4, 'mramor'),
       (5, 'ocel'),
       (6, 'hliník'),
       (7, 'sklo');
/*!40000 ALTER TABLE `materials`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `cancelled`   bit(1)  NOT NULL,
    `fulfilled`   bit(1)  NOT NULL,
    `order_date`  date         DEFAULT NULL,
    `paid`        bit(1)  NOT NULL,
    `ship_date`   date         DEFAULT NULL,
    `total_price` decimal(19,
                      2)       DEFAULT NULL,
    `total_tax`   decimal(19,
                      2)       DEFAULT NULL,
    `uuid`        varchar(255) DEFAULT NULL,
    `address_id`  int(11)      DEFAULT NULL,
    `user_id`     int(11)      DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            ),
    KEY `FKebp0nos59lx5w85w9pwsyfm1v`
        (
         `address_id`
            ),
    KEY `FKel9kyl84ego2otj2accfd8mr7`
        (
         `user_id`
            ),
    CONSTRAINT `FKebp0nos59lx5w85w9pwsyfm1v` FOREIGN KEY
        (
         `address_id`
            ) REFERENCES `adresses`
            (
             `id`
                ),
    CONSTRAINT `FKel9kyl84ego2otj2accfd8mr7` FOREIGN KEY
        (
         `user_id`
            ) REFERENCES `user`
            (
             `id`
                )
) ENGINE = InnoDB
  AUTO_INCREMENT = 11
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.orders: ~10 rows (přibližně)
/*!40000 ALTER TABLE `orders`
    DISABLE KEYS */;
INSERT INTO `orders` (`id`, `cancelled`, `fulfilled`, `order_date`, `paid`, `ship_date`, `total_price`, `total_tax`,
                      `uuid`, `address_id`, `user_id`)
VALUES (1, b'0', b'1', '2022-01-14', b'1', '2022-01-14', 7333.00, 1272.67, '3149b9e6-c', 10, 1),
       (2, b'0', b'0', '2022-01-14', b'0', NULL, 15433.00, 2678.45, '15ec61dd-a', 11, 1),
       (3, b'0', b'0', '2022-01-14', b'0', NULL, 33000.00, 5727.27, 'e06ed8aa-4', 12, 9),
       (4, b'0', b'0', '2022-01-14', b'1', NULL, 5000.00, 867.77, '929f7f6f-c', 13, 9),
       (5, b'0', b'0', '2022-01-14', b'0', NULL, 4305.00, 747.15, '299e8ba0-2', 14, 8),
       (6, b'0', b'0', '2022-01-14', b'0', NULL, 15000.00, 2603.30, 'b7aa0f0a-9', 15, 8),
       (7, b'0', b'0', '2022-01-14', b'1', NULL, 13249.00, 2299.42, '233cae28-e', 16, 7),
       (8, b'0', b'0', '2022-01-14', b'0', NULL, 3300.00, 572.73, '80a9bc1e-d', 17, 7),
       (9, b'0', b'1', '2022-01-14', b'0', '2022-01-14', 35000.00, 6074.38, '88fe6e50-c', 18, 6),
       (10, b'0', b'0', '2022-01-14', b'0', NULL, 50000.00, 8677.68, '8f04ebcc-0', 19, 6);
/*!40000 ALTER TABLE `orders`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.order_details
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details`
(
    `id`                int(11) NOT NULL AUTO_INCREMENT,
    `quantity`          int(11) NOT NULL,
    `order_id`          int(11) DEFAULT NULL,
    `product_detail_id` int(11) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            ),
    KEY `FKjyu2qbqt8gnvno9oe9j2s2ldk`
        (
         `order_id`
            ),
    KEY `FKl9w7hjxo9qo8s0glyr4ngkm1d`
        (
         `product_detail_id`
            ),
    CONSTRAINT `FKjyu2qbqt8gnvno9oe9j2s2ldk` FOREIGN KEY
        (
         `order_id`
            ) REFERENCES `orders`
            (
             `id`
                ),
    CONSTRAINT `FKl9w7hjxo9qo8s0glyr4ngkm1d` FOREIGN KEY
        (
         `product_detail_id`
            ) REFERENCES `product_details`
            (
             `id`
                )
) ENGINE = InnoDB
  AUTO_INCREMENT = 21
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.order_details: ~20 rows (přibližně)
/*!40000 ALTER TABLE `order_details`
    DISABLE KEYS */;
INSERT INTO `order_details` (`id`, `quantity`, `order_id`, `product_detail_id`)
VALUES (1, 1, 1, 4),
       (2, 2, 1, 10),
       (3, 1, 2, 23),
       (4, 1, 2, 12),
       (5, 1, 3, 6),
       (6, 1, 3, 11),
       (7, 1, 4, 11),
       (8, 1, 4, 15),
       (9, 1, 5, 16),
       (10, 1, 5, 24),
       (11, 1, 6, 21),
       (12, 1, 6, 12),
       (13, 1, 7, 7),
       (14, 1, 7, 25),
       (15, 1, 8, 10),
       (16, 1, 8, 1),
       (17, 1, 9, 9),
       (18, 1, 9, 8),
       (19, 1, 10, 5),
       (20, 1, 10, 8);
/*!40000 ALTER TABLE `order_details`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.photo_storage
DROP TABLE IF EXISTS `photo_storage`;
CREATE TABLE IF NOT EXISTS `photo_storage`
(
    `id`                int(11) NOT NULL AUTO_INCREMENT,
    `file_path`         varchar(255) DEFAULT NULL,
    `product_detail_id` int(11)      DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            ),
    KEY `FKa51q49vwwhnv43h2b571e6nw9`
        (
         `product_detail_id`
            ),
    CONSTRAINT `FKa51q49vwwhnv43h2b571e6nw9` FOREIGN KEY
        (
         `product_detail_id`
            ) REFERENCES `product_details`
            (
             `id`
                )
) ENGINE = InnoDB
  AUTO_INCREMENT = 26
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.photo_storage: ~25 rows (přibližně)
/*!40000 ALTER TABLE `photo_storage`
    DISABLE KEYS */;
INSERT INTO `photo_storage` (`id`, `file_path`, `product_detail_id`)
VALUES (1, '../product_images/024455aa-e.jpg', 1),
       (2, '../product_images/cbce0883-9.jpg', 2),
       (3, '../product_images/1567f014-6.jpg', 3),
       (4, '../product_images/5227128e-e.jpg', 4),
       (5, '../product_images/3af879c7-1.jpg', 5),
       (6, '../product_images/3f802f97-b.jpg', 6),
       (7, '../product_images/4e72ec9a-6.jpg', 7),
       (8, '../product_images/5f1399a8-1.jpg', 8),
       (9, '../product_images/6ad2b476-d.jpg', 9),
       (10, '../product_images/bdc52163-b.jpg', 10),
       (11, '../product_images/24dfd7c0-8.jpg', 11),
       (12, '../product_images/cc6e49d6-5.jpg', 12),
       (13, '../product_images/18dd3ca8-d.jpg', 13),
       (14, '../product_images/15ca5859-8.jpg', 14),
       (15, '../product_images/c85fe4d5-b.jpg', 15),
       (16, '../product_images/4879463d-2.jpg', 16),
       (17, '../product_images/c627107a-a.jpg', 17),
       (18, '../product_images/6686468d-e.jpg', 18),
       (19, '../product_images/272891bb-4.jpg', 19),
       (20, '../product_images/43354645-b.jpg', 20),
       (21, '../product_images/d4e4f4d4-6.jpg', 21),
       (22, '../product_images/25fe2bde-5.jpg', 22),
       (23, '../product_images/b6d10861-8.jpg', 23),
       (24, '../product_images/a20dfe23-4.jpg', 24),
       (25, '../product_images/021f2812-f.jpg', 25);
/*!40000 ALTER TABLE `photo_storage`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `description` varchar(255) DEFAULT NULL,
    `name`        varchar(255) DEFAULT NULL,
    `photo`       varchar(255) DEFAULT NULL,
    `category_id` int(11)      DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            ),
    KEY `FKog2rp4qthbtt2lfyhfo32lsw9`
        (
         `category_id`
            ),
    CONSTRAINT `FKog2rp4qthbtt2lfyhfo32lsw9` FOREIGN KEY
        (
         `category_id`
            ) REFERENCES `categories`
            (
             `id`
                )
) ENGINE = InnoDB
  AUTO_INCREMENT = 22
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.products: ~21 rows (přibližně)
/*!40000 ALTER TABLE `products`
    DISABLE KEYS */;
INSERT INTO `products` (`id`, `description`, `name`, `photo`, `category_id`)
VALUES (1,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Židle 1', '../product_images/024455aa-e.jpg', 2),
       (2,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Židle 2', '../product_images/1567f014-6.jpg', 2),
       (3,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Postel 1', '../product_images/3af879c7-1.jpg', 6),
       (4,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Postel 2', '../product_images/4e72ec9a-6.jpg', 6),
       (5,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Stůl 1', '../product_images/6ad2b476-d.jpg', 1),
       (6,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Stůl 2', '../product_images/bdc52163-b.jpg', 1),
       (7,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Skříň 1', '../product_images/24dfd7c0-8.jpg', 5),
       (8,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Skříň 2', '../product_images/cc6e49d6-5.jpg', 5),
       (9,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Komoda 1', '../product_images/18dd3ca8-d.jpg', 3),
       (10,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Komoda 2', '../product_images/15ca5859-8.jpg', 3),
       (11,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Stůl 3', '../product_images/c85fe4d5-b.jpg', 1),
       (12,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Židle 3', '../product_images/4879463d-2.jpg', 2),
       (13,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Postel 3', '../product_images/c627107a-a.jpg', 6),
       (14,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Komoda 3', '../product_images/6686468d-e.jpg', 3),
       (15,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Dveře 1', '../product_images/272891bb-4.jpg', 4),
       (16,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Dveře 2', '../product_images/43354645-b.jpg', 4),
       (17,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Dveře 3', '../product_images/d4e4f4d4-6.jpg', 4),
       (18,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Dveře 4', '../product_images/25fe2bde-5.jpg', 4),
       (19,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Okno 1', '../product_images/b6d10861-8.jpg', 7),
       (20,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Okno 2', '../product_images/a20dfe23-4.jpg', 7),
       (21,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam in imperdiet pharetra. Ut finibus augue eget orci finibus, sit amet aliquet felis semper. Donec non egestas dolor.',
        'Okno 3', '../product_images/021f2812-f.jpg', 7);
/*!40000 ALTER TABLE `products`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.products_available_materials
DROP TABLE IF EXISTS `products_available_materials`;
CREATE TABLE IF NOT EXISTS `products_available_materials`
(
    `product_id`             int(11) NOT NULL,
    `available_materials_id` int(11) NOT NULL,
    KEY `FKtrkpmiwljkkx5gpfh1wme3tgp`
        (
         `available_materials_id`
            ),
    KEY `FKdhnlc4jb4mssst2wa80ll903h`
        (
         `product_id`
            ),
    CONSTRAINT `FKdhnlc4jb4mssst2wa80ll903h` FOREIGN KEY
        (
         `product_id`
            ) REFERENCES `products`
            (
             `id`
                ),
    CONSTRAINT `FKtrkpmiwljkkx5gpfh1wme3tgp` FOREIGN KEY
        (
         `available_materials_id`
            ) REFERENCES `materials`
            (
             `id`
                )
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.products_available_materials: ~25 rows (přibližně)
/*!40000 ALTER TABLE `products_available_materials`
    DISABLE KEYS */;
INSERT INTO `products_available_materials` (`product_id`, `available_materials_id`)
VALUES (1, 1),
       (1, 2),
       (2, 5),
       (2, 6),
       (3, 1),
       (3, 5),
       (4, 6),
       (4, 3),
       (5, 1),
       (6, 2),
       (7, 5),
       (8, 1),
       (9, 6),
       (10, 2),
       (11, 3),
       (12, 5),
       (13, 5),
       (14, 3),
       (15, 1),
       (16, 5),
       (17, 6),
       (18, 4),
       (19, 1),
       (20, 7),
       (21, 2);
/*!40000 ALTER TABLE `products_available_materials`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.products_available_shapes
DROP TABLE IF EXISTS `products_available_shapes`;
CREATE TABLE IF NOT EXISTS `products_available_shapes`
(
    `product_id`          int(11) NOT NULL,
    `available_shapes_id` int(11) NOT NULL,
    KEY `FKlhvnyegg2ubjauhdntsuufbcb`
        (
         `available_shapes_id`
            ),
    KEY `FK1860mlakpeyhgf9amh5ocjhn4`
        (
         `product_id`
            ),
    CONSTRAINT `FK1860mlakpeyhgf9amh5ocjhn4` FOREIGN KEY
        (
         `product_id`
            ) REFERENCES `products`
            (
             `id`
                ),
    CONSTRAINT `FKlhvnyegg2ubjauhdntsuufbcb` FOREIGN KEY
        (
         `available_shapes_id`
            ) REFERENCES `shapes`
            (
             `id`
                )
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.products_available_shapes: ~25 rows (přibližně)
/*!40000 ALTER TABLE `products_available_shapes`
    DISABLE KEYS */;
INSERT INTO `products_available_shapes` (`product_id`, `available_shapes_id`)
VALUES (1, 2),
       (1, 1),
       (2, 1),
       (2, 3),
       (3, 2),
       (3, 4),
       (4, 4),
       (4, 1),
       (5, 3),
       (6, 1),
       (7, 4),
       (8, 1),
       (9, 4),
       (10, 4),
       (11, 2),
       (12, 1),
       (13, 3),
       (14, 1),
       (15, 4),
       (16, 3),
       (17, 2),
       (18, 4),
       (19, 2),
       (20, 4),
       (21, 1);
/*!40000 ALTER TABLE `products_available_shapes`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.product_details
DROP TABLE IF EXISTS `product_details`;
CREATE TABLE IF NOT EXISTS `product_details`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `depth`          float   NOT NULL CHECK
        (
        `depth` >= 0
        ),
    `height`         float   NOT NULL CHECK
        (
        `height` >= 0
        ),
    `tax`            decimal(19,
                         2)  NOT NULL,
    `taxation`       float   NOT NULL,
    `unit_price`     decimal(19,
                         2)  NOT NULL CHECK
        (
        `unit_price` >= 0
        ),
    `units_in_stock` int(11) NOT NULL CHECK
        (
        `units_in_stock` >= 0
        ),
    `weight`         float   NOT NULL CHECK
        (
        `weight` >= 0
        ),
    `width`          float   NOT NULL CHECK
        (
        `width` >= 0
        ),
    `material_id`    int(11) DEFAULT NULL,
    `product_id`     int(11) DEFAULT NULL,
    `shape_id`       int(11) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            ),
    KEY `FKq2yn5xt7o49ewjor6jf69x19d`
        (
         `material_id`
            ),
    KEY `FKnfvvq3meg4ha3u1bju9k4is3r`
        (
         `product_id`
            ),
    KEY `FK99ncq4bwsfp85r1baekrbbru5`
        (
         `shape_id`
            ),
    CONSTRAINT `FK99ncq4bwsfp85r1baekrbbru5` FOREIGN KEY
        (
         `shape_id`
            ) REFERENCES `shapes`
            (
             `id`
                ),
    CONSTRAINT `FKnfvvq3meg4ha3u1bju9k4is3r` FOREIGN KEY
        (
         `product_id`
            ) REFERENCES `products`
            (
             `id`
                ),
    CONSTRAINT `FKq2yn5xt7o49ewjor6jf69x19d` FOREIGN KEY
        (
         `material_id`
            ) REFERENCES `materials`
            (
             `id`
                )
) ENGINE = InnoDB
  AUTO_INCREMENT = 26
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.product_details: ~25 rows (přibližně)
/*!40000 ALTER TABLE `product_details`
    DISABLE KEYS */;
INSERT INTO `product_details` (`id`, `depth`, `height`, `tax`, `taxation`, `unit_price`, `units_in_stock`, `weight`,
                               `width`, `material_id`, `product_id`, `shape_id`)
VALUES (1, 100, 100, 225.62, 0.21, 1300.00, 19, 10, 200, 1, 1, 2),
       (2, 30, 30, 208.26, 0.21, 1200.00, 10, 5, 30, 2, 1, 1),
       (3, 30, 90, 212.08, 0.21, 1222.00, 50, 2, 30, 5, 2, 1),
       (4, 30, 50, 578.45, 0.21, 3333.00, 99, 20, 20, 6, 2, 3),
       (5, 200, 50, 3471.07, 0.21, 20000.00, 99, 100, 200, 1, 3, 2),
       (6, 180, 30, 5206.61, 0.21, 30000.00, 199, 80, 180, 5, 3, 4),
       (7, 100, 20, 1735.54, 0.21, 10000.00, 89, 40, 100, 6, 4, 4),
       (8, 100, 80, 5206.61, 0.21, 30000.00, 18, 100, 100, 3, 4, 1),
       (9, 34, 60, 867.77, 0.21, 5000.00, 19, 12, 30, 1, 5, 3),
       (10, 30, 30, 347.11, 0.21, 2000.00, 7, 4, 30, 2, 6, 1),
       (11, 30, 200, 520.66, 0.21, 3000.00, 3, 10, 30, 5, 7, 4),
       (12, 20, 180, 2082.64, 0.21, 12000.00, 8, 30, 200, 1, 8, 1),
       (13, 20, 30, 347.11, 0.21, 2000.00, 20, 20, 200, 6, 9, 4),
       (14, 20, 50, 347.11, 0.21, 2000.00, 10, 10, 20, 2, 10, 4),
       (15, 30, 30, 347.11, 0.21, 2000.00, 15, 10, 30, 3, 11, 2),
       (16, 20, 30, 347.11, 0.21, 2000.00, 13, 1, 20, 5, 12, 1),
       (17, 200, 20, 3471.07, 0.21, 20000.00, 2, 90, 200, 5, 13, 3),
       (18, 20, 300, 694.21, 0.21, 4000.00, 4, 20, 20, 3, 14, 1),
       (19, 2, 200, 347.11, 0.21, 2000.00, 6, 4, 50, 1, 15, 4),
       (20, 5, 150, 416.53, 0.21, 2400.00, 7, 20, 200, 5, 16, 3),
       (21, 5, 50, 520.66, 0.21, 3000.00, 1, 20, 50, 6, 17, 2),
       (22, 3, 60, 987.52, 0.21, 5690.00, 6, 30, 60, 4, 18, 4),
       (23, 10, 30, 595.81, 0.21, 3433.00, 2, 5, 30, 1, 19, 2),
       (24, 2, 40, 400.04, 0.21, 2305.00, 1, 10, 20, 7, 20, 4),
       (25, 20, 50, 563.88, 0.21, 3249.00, 29, 10, 30, 2, 21, 1);
/*!40000 ALTER TABLE `product_details`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.review
DROP TABLE IF EXISTS `review`;
CREATE TABLE IF NOT EXISTS `review`
(
    `id`             int(11)        NOT NULL AUTO_INCREMENT,
    `score`          decimal(19, 2) NOT NULL CHECK (`score` <= 5 and `score` >= 0),
    `text`           varchar(255) DEFAULT NULL,
    `rew_product_id` int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKrmhofp37yjoiahre6bmtbm74s` (`rew_product_id`),
    CONSTRAINT `FKrmhofp37yjoiahre6bmtbm74s` FOREIGN KEY (`rew_product_id`) REFERENCES `products` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.review: ~0 rows (přibližně)
/*!40000 ALTER TABLE `review`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `review`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role`
(
    `id`               int(11) NOT NULL AUTO_INCREMENT,
    `name`             varchar(255) DEFAULT NULL,
    `role_description` varchar(255) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            )
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.role: ~2 rows (přibližně)
/*!40000 ALTER TABLE `role`
    DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `role_description`)
VALUES (1, 'admin', 'administrator'),
       (2, 'user', 'user');
/*!40000 ALTER TABLE `role`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.shapes
DROP TABLE IF EXISTS `shapes`;
CREATE TABLE IF NOT EXISTS `shapes`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            )
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.shapes: ~4 rows (přibližně)
/*!40000 ALTER TABLE `shapes`
    DISABLE KEYS */;
INSERT INTO `shapes` (`id`, `name`)
VALUES (1, 'kruh'),
       (2, 'čtverec'),
       (3, 'trojúhelník'),
       (4, 'obdélník');
/*!40000 ALTER TABLE `shapes`
    ENABLE KEYS */;

-- Exportování struktury pro tabulka eshop.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `dic`          varchar(255) DEFAULT NULL,
    `email`        varchar(255) DEFAULT NULL,
    `enabled`      bit(1)       NOT NULL,
    `ico`          int(11)      NOT NULL,
    `name`         varchar(255) DEFAULT NULL,
    `password`     varchar(255) DEFAULT NULL,
    `phone_number` varchar(255) NOT NULL,
    `surname`      varchar(255) DEFAULT NULL,
    `username`     varchar(255) DEFAULT NULL,
    `address_id`   int(11)      NOT NULL,
    `cart_id`      int(11)      DEFAULT NULL,
    `role_id`      int(11)      DEFAULT NULL,
    PRIMARY KEY
        (
         `id`
            ),
    UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx`
        (
         `username`
            ),
    KEY `FKkvrqfguw3ulmm5q9o09k8er6t`
        (
         `address_id`
            ),
    KEY `FKtqa69bib34k2c0jhe7afqsao6`
        (
         `cart_id`
            ),
    KEY `FKn82ha3ccdebhokx3a8fgdqeyy`
        (
         `role_id`
            ),
    CONSTRAINT `FKkvrqfguw3ulmm5q9o09k8er6t` FOREIGN KEY
        (
         `address_id`
            ) REFERENCES `adresses`
            (
             `id`
                ),
    CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY
        (
         `role_id`
            ) REFERENCES `role`
            (
             `id`
                ),
    CONSTRAINT `FKtqa69bib34k2c0jhe7afqsao6` FOREIGN KEY
        (
         `cart_id`
            ) REFERENCES `cart`
            (
             `id`
                )
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8mb3;

-- Exportování dat pro tabulku eshop.user: ~9 rows (přibližně)
/*!40000 ALTER TABLE `user`
    DISABLE KEYS */;
INSERT INTO `user` (`id`, `dic`, `email`, `enabled`, `ico`, `name`, `password`, `phone_number`, `surname`, `username`,
                    `address_id`, `cart_id`, `role_id`)
VALUES (1, '', 'dom@mail.com', b'1', 0, 'Dominik', '$2a$10$Oy5nHOwBUMZ3GOKPGd5dPOv35OAiHhqo4/hDzyOUEnh.p/YMVSAuC',
        '123456789', 'Búzik', 'dom', 1, 1, 1),
       (2, NULL, 'jan@mail.com', b'1', 0, 'Jan', '$2a$10$6GOUjahn8vfTYo76pNcFwun0s20c1JZX6/7oOKECT3JnfKiKTk5nG',
        '123456789', 'Novák', 'nov1', 2, 2, 2),
       (3, '', 'nov1@mail.com', b'1', 0, 'Adam', '$2a$10$i.x/WngiAtOWix855ivsB.P4y8WdndNU3caCqnUp7c/mr3uGN9Tfq',
        '876879987', 'Nový', 'nova', 3, 3, 2),
       (4, 'CZ652652', 'pardh@centrum.cz', b'1', 652652, 'Hana',
        '$2a$10$EHt3304UVV0hFEne0x9bPObN4pYnzzZoA7Lky5AtEvMx5GWJlG5/C', '986787563', 'Pardubická', 'pardh', 4, 4, 2),
       (5, '', 'doe@asdl.com', b'1', 0, 'John', '$2a$10$6dDw7T5SxMBe1wHFlOjCCe03sXVvI0MnYhr/EocV41hsNw90Fn6mS',
        '765872568', 'Doe', 'doe', 5, 5, 2),
       (6, '', 'prvak@mail.com', b'1', 0, 'Pavel', '$2a$10$bks/HIYw9O0TNXtvughTc.Vr10LXtSlei.Gm7xhiLZD4h2z19U/7O',
        '567287276', 'Prvák', 'prvak', 6, 6, 2),
       (7, '', 'druhak@centrum.cz', b'1', 0, 'Jan', '$2a$10$SO/1NdkTiw3fYQNff9trpuT/CNg7/zjzEjP4fRWLfHLwOREG2QOY6',
        '869866873', 'Druhák', 'druhak', 7, 7, 2),
       (8, '', 'treti@aol.com', b'1', 0, 'Michal', '$2a$10$H57uKJIxfCoUInzFB3EPWuuFHiV7xEst00.KGLS8eqiVeuaYii72O',
        '876456890', 'Třetí', 'treti', 8, 8, 2),
       (9, '', 'ctverak@mail.ru', b'1', 0, 'Jakub', '$2a$10$oefFzosHZZKPVYr5sPir0e6.UgICBr4aUGGcWciV76zaz/oyEBpYu',
        '762762872', 'Čtverák', 'ctverak', 9, 9, 2);
/*!40000 ALTER TABLE `user`
    ENABLE KEYS */;

/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES = IFNULL(@OLD_SQL_NOTES, 1) */;
