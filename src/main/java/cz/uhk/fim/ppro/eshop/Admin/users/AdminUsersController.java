package cz.uhk.fim.ppro.eshop.Admin.users;

import cz.uhk.fim.ppro.eshop.Model.User;
import cz.uhk.fim.ppro.eshop.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin/users")
public class AdminUsersController {
    private UserService userService;
    private User currentUser;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public AdminUsersController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = {"/", "/index"})
    public String index(Model model, @RequestParam("page") Optional<Integer> page,
                        @RequestParam("size") Optional<Integer> size, @RequestParam("searchText") Optional<String> searchText) {

        currentUser = userService.getCurrentUser().orElse(null);
        model.addAttribute("user", currentUser);


        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = (int) userService.count();
        int pages = (int) Math.ceil(totalItems / (double) pageSize);
        Pageable pageAble = PageRequest.of(currentPage - 1, pageSize);
        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", currentPage);
        //vyhledavani
        if (searchText.isPresent()) {
            List<User> lstSearchUsers = userService.getAllBySurnameIsContainingOrUsernameIsContaining(searchText.get(), searchText.get());
            if (!lstSearchUsers.isEmpty()) {
                model.addAttribute("users", lstSearchUsers);
            }
        } else {
            model.addAttribute("users", userService.findAll(pageAble).getContent());
        }
        return "admin/users/index";
    }

    @RequestMapping("/deactivateUser")
    public String deactivateUser(@RequestParam("id") int id, RedirectAttributes redirectAttributes) {
        User user = userService.getOne(id);
        user.setEnabled(false);
        userService.save(user);
        redirectAttributes.addAttribute("user_deactivate_success");
        return "redirect:/admin/users/";
    }

    @GetMapping("/changePswd")
    public String changePswd(@RequestParam("id") int id, Model model) {
        User user = userService.getOne(id);
        model.addAttribute("user", user);
        return "admin/users/changePswd";
    }

    @PostMapping("/postChangePswd")
    public String postChangePswd(User u, RedirectAttributes redirectAttributes) {

        User user = userService.getOne(u.getId());
        user.setPassword(passwordEncoder.encode(u.getPassword()));
        userService.save(user);
        redirectAttributes.addAttribute("user_update_success", true);
        return "redirect:/admin/users/";
    }
}
