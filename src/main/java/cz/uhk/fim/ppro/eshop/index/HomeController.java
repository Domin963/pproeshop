package cz.uhk.fim.ppro.eshop.index;

import cz.uhk.fim.ppro.eshop.Interfaces.IAddressRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.IRoleRepository;
import cz.uhk.fim.ppro.eshop.Model.User;
import cz.uhk.fim.ppro.eshop.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@Controller
public class HomeController {
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    private UserService userService;
    @Autowired
    private IAddressRepository addressRepository;

    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = {"/", "/index"})
    public String index() {
        return "index";
    }

    @GetMapping(value = {"/make"})
    public String make(Model model, @RequestParam(value = "productName", required=false) String productName) {
        model.addAttribute("product",productName);
        return "make";
    }

    @GetMapping(value = {"/contact"})
    public String contact() {
        return "contact";
    }

    @GetMapping(value = {"/review"})
    public String info() {
        return "review";
    }

    @GetMapping(value = "/login")
    public String loginGet(Model model, String error) {
        //overeni loginu dela Spring Security, dle nastaveni v/Security
        if (error != null)
            model.addAttribute("error", "Nesprávné jméno/heslo!");
        return "login";
    }


    /**
     * Method for deciding which index will be used based on user's role
     *
     * @return redirect to correct /index
     */
    @RequestMapping(value = "/defaultAfterLogin")
    public String defaultAfterLogin() {
        if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority("admin"))) {
            return "redirect:admin/";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/register")
    public String registerGet(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "register";
    }

    @PostMapping(value = "/postRegister")
    public String registerPost(@Valid @ModelAttribute User user, BindingResult bindingResult) {
        user.setRole(roleRepository.getByName("user").orElseThrow(() -> new UsernameNotFoundException("Role not found")));
        if (bindingResult.hasErrors()) {
            return "register";
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAddress(addressRepository.save(user.getAddress()));
        userService.save(user);
        return "login";
    }
}
