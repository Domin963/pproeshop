package cz.uhk.fim.ppro.eshop.Orders;


import cz.uhk.fim.ppro.eshop.Interfaces.*;
import cz.uhk.fim.ppro.eshop.Model.*;
import cz.uhk.fim.ppro.eshop.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/orders")
public class OrdersController {
    private UserService userService;
    @Autowired
    private IAddressRepository addressRepository;
    @Autowired
    private IOrderRepository orderRepository;
    @Autowired
    private IOrderDetailRepository orderDetailRepository;
    @Autowired
    private IProductDetailRepository productDetailRepository;
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IReviewRepository reviewRepository;

    public OrdersController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Method for showing all Orders made by currently logged in user
     *
     * @param model
     * @return orders/index
     */
    @GetMapping(value = {"/", "/index", ""})
    public String index(Model model, @RequestParam("page") Optional<Integer> page,
                        @RequestParam("size") Optional<Integer> size) {
        User user = userService.getCurrentUser().orElse(null);
        List<Order> orders = orderRepository.getAllByUserId(user.getId());
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = (int) orderRepository.getAllByUserId(user.getId()).stream().count();
        int pages = (int) Math.ceil(totalItems / (double) pageSize);
        Pageable pageAble = PageRequest.of(currentPage - 1, pageSize);
        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("orders", orders);
        return "orders/index";
    }

    @GetMapping("/detail")
    public String detail(@RequestParam("id") int id, Model model) {
        Order order = orderRepository.getOne(id);
        List<OrderDetail> lstOrderDetail = orderDetailRepository.getAllByOrder(order);
        model.addAttribute("lstOrderDetail", lstOrderDetail);
        model.addAttribute("order", order);
        return "orders/detail";
    }

    /**
     * Method for filling in delivery and billing info about user
     *
     * @param model
     * @return orders/create
     */
    @GetMapping("/createOrder")
    public String createOrder(Model model) {
        User user = userService.getCurrentUser().orElse(null);
        if (user.getCart().getCartProducts().isEmpty()) {
            return "redirect:/cart/index";
        }
        Address address = user.getAddress();
        model.addAttribute("user", user);
        model.addAttribute("address", address);
        return "orders/create";
    }

    @PostMapping("/postOrder")
    public String postOrder(@Valid @ModelAttribute User user, BindingResult bindingResult1, @Valid @ModelAttribute Address address, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors() || bindingResult1.hasErrors()) {
            return "orders/create";
        }
        User currentUser = userService.getCurrentUser().orElse(null);
        user.setAddress(address);
        model.addAttribute("user", user);
        model.addAttribute("cart", currentUser.getCart());
        return "orders/orderSummary";
    }

    @RequestMapping("/finishOrder")
    public String finishOrder(@ModelAttribute User user, RedirectAttributes redirectAttributes) {
        Order order = new Order();
        List<OrderDetail> orderDetails = new ArrayList<>();
        User currentUser = userService.getCurrentUser().orElse(null);
        for (CartProduct x : currentUser.getCart().getCartProducts()) {
            ProductDetail prodDetail = productDetailRepository.getOne(x.getProductDetail().getId());
            //finalni kontrola, jestli je dost veci na sklade
            if (x.getQuantity() >= prodDetail.getUnitsInStock()) {
                redirectAttributes.addAttribute("error_not_enough_stock", true);
                return "redirect:cart/index";
            }
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrder(order);
            orderDetail.setProductDetail(prodDetail);
            orderDetail.setQuantity(x.getQuantity());
            orderDetails.add(orderDetail);
        }
        //zrejme je vseho dostatek
        //aktualizace poctu veci na sklade
        List<ProductDetail> productDetails = new ArrayList<>();
        for (OrderDetail x : orderDetails) {
            ProductDetail productDetail = productDetailRepository.getOne(x.getProductDetail().getId());
            productDetail.setUnitsInStock(productDetail.getUnitsInStock() - x.getQuantity());
            productDetails.add(productDetail);
        }
        productDetailRepository.saveAll(productDetails);
        //uprava uzivatele, pokud se neco zmenilo
        currentUser.setName(user.getName());
        currentUser.setSurname(user.getSurname());
        currentUser.setEmail(user.getEmail());
        currentUser.setPhoneNumber(user.getPhoneNumber());
        currentUser.setDic(user.getDic());
        currentUser.setIco(user.getIco());

        order.setAddress(new Address());
        order.getAddress().setCity(user.getAddress().getCity());
        order.getAddress().setStreetNameHouseNum(user.getAddress().getStreetNameHouseNum());
        order.getAddress().setPostalCode(user.getAddress().getPostalCode());
        order.setAddress(addressRepository.save(order.getAddress()));
        //vyprazdneni kosiku
        currentUser.getCart().getCartProducts().clear();
        userService.save(currentUser);

        order.setUser(currentUser);
        orderRepository.save(order);

        order.setOrderDetails(orderDetails);
        order.calculateTotalPrice();

        orderDetailRepository.saveAll(orderDetails);
        orderRepository.save(order);
        redirectAttributes.addAttribute("order_create_success", true);
        return "redirect:/products/index";
    }

    @GetMapping("/addReview")
    public String addReview(@RequestParam("id") int id, Model model) {
        model.addAttribute("id", id);
        return "orders/review";
    }

    @PostMapping("/addReviewPost")
    public String addReviewPost(@ModelAttribute Review rview, @RequestParam("id") int id) {
        Review review = new Review();
        review.setRewProduct(productRepository.getOne(id));
        review.setScore(rview.getScore());
        review.setText(rview.getText());
        reviewRepository.save(review);
        return "redirect:/orders";
    }
}
