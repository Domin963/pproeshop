package cz.uhk.fim.ppro.eshop.Model;

import javax.persistence.*;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne()
    private User user;
    @ManyToOne
    @Valid
    private Address address;
    private LocalDate orderDate;
    private String uuid;
    private boolean fulfilled;
    private boolean paid;
    private BigDecimal totalPrice;
    private BigDecimal totalTax;
    private LocalDate shipDate;
    private boolean cancelled;
    @OneToMany(mappedBy = "order")
    private List<OrderDetail> orderDetails;

    public Order() {
        uuid = UUID.randomUUID().toString().substring(0, 10);
        orderDetails = new ArrayList<>();
        orderDate = LocalDate.now();
        fulfilled = false;
        paid = false;
        cancelled = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public boolean isFulfilled() {
        return fulfilled;
    }

    public void setFulfilled(boolean fulfilled) {
        this.fulfilled = fulfilled;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDate getShipDate() {
        return shipDate;
    }

    public void setShipDate(LocalDate shipDate) {
        this.shipDate = shipDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    /**
     * Method for calculating total price
     */
    public void calculateTotalPrice() {
        totalPrice = BigDecimal.valueOf(0);
        totalTax = BigDecimal.valueOf(0);
        for (OrderDetail o : orderDetails
        ) {
            totalPrice = totalPrice.add(o.getProductDetail().getUnitPrice().multiply(BigDecimal.valueOf(o.getQuantity())));
            totalTax = totalTax.add(o.getProductDetail().getTax().multiply((BigDecimal.valueOf(o.getQuantity()))));
        }
        //round final TotalPrice to whole number, tax to 2 significant digits
        totalPrice = totalPrice.setScale(0, BigDecimal.ROUND_HALF_EVEN);
        totalTax = totalTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

}
