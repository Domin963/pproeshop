package cz.uhk.fim.ppro.eshop.Repositories;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.ExtRepo;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;

public class ExtRepoImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements ExtRepo<T, ID> {
    private EntityManager entityManager;
    private CriteriaBuilder builder;

    public ExtRepoImpl(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }
}
