package cz.uhk.fim.ppro.eshop.Interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
@NoRepositoryBean
public interface ExtRepo<T, ID extends Serializable> extends JpaRepository<T, ID> {
}
