package cz.uhk.fim.ppro.eshop.User;

import cz.uhk.fim.ppro.eshop.Interfaces.IProductDetailRepository;
import cz.uhk.fim.ppro.eshop.Model.User;
import cz.uhk.fim.ppro.eshop.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    IProductDetailRepository productDetailRepository;
    private UserService userService;
    private User user;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {

        user = userService.getCurrentUser().orElse(null);
        model.addAttribute("user", user);

        return "/user/index";
    }

    @PostMapping(value = "/editUser")
    public String editUser(@Valid @ModelAttribute User user, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "user/index";
        }
        User u = userService.getOne(user.getId());
        u.setName(user.getName());
        u.setSurname(user.getSurname());
        u.setEmail(user.getEmail());
        u.setPhoneNumber(user.getPhoneNumber());
        u.getAddress().setStreetNameHouseNum(user.getAddress().getStreetNameHouseNum());
        u.getAddress().setCity(user.getAddress().getCity());
        u.getAddress().setPostalCode(user.getAddress().getPostalCode());
        userService.save(u);
        redirectAttributes.addAttribute("user_change_success", true);
        return "redirect:/user/index";
    }

    @GetMapping(value = "/changePswd")
    public String changePswd(Model model) {
        User user = userService.getCurrentUser().orElse(null);
        user.setPassword("");
        model.addAttribute("user", user);
        return "/user/changePswd";
    }

    @PostMapping(value = "/postChangePswd")
    public String postChangePswd(@Valid @ModelAttribute User user, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "user/changePswd";
        }
        User u = userService.getCurrentUser().orElse(null);
        u.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(u);
        redirectAttributes.addAttribute("user_change_success", true);
        return "redirect:/user/index";
    }
}