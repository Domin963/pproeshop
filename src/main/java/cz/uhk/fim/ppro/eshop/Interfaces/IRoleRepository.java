package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Role;

import java.util.Optional;

public interface IRoleRepository extends ExtRepo<Role, Integer> {
    Optional<Role> getByName(String roleName);
}
