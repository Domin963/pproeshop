package cz.uhk.fim.ppro.eshop.Carts;

import cz.uhk.fim.ppro.eshop.Interfaces.IProductDetailRepository;
import cz.uhk.fim.ppro.eshop.Model.CartProduct;
import cz.uhk.fim.ppro.eshop.Model.User;
import cz.uhk.fim.ppro.eshop.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/cart")
public class CartsController {
    @Autowired
    IProductDetailRepository productDetailRepository;
    private UserService userService;
    private User user;

    public CartsController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = {"", "/", "/index"})
    public String index(Model model) {
        user = userService.getCurrentUser().orElse(null);
        model.addAttribute("cart", user.getCart());
        return "/cart/index";
    }

    /**
     * Adds item to cart
     *
     * @param productDetailId ProductDetail id of item to be added
     * @param quantity        quantity of items to be added
     * @return
     */
    @PostMapping("/addToCart")
    public String addToCart(@RequestParam("productDetailId") int productDetailId, @RequestParam("quantity") int quantity, RedirectAttributes redirectAttributes) {
        user = userService.getCurrentUser().orElse(null);
        int unitsInStock = productDetailRepository.getOne(productDetailId).getUnitsInStock();
        //pokud uz v kosiku takova vec je, jen ji zvysime quantity
        if (user.getCart().getByProductDetailId(productDetailId) != null) {
            quantity += user.getCart().getByProductDetailId(productDetailId).getQuantity();
            if (unitsInStock >= quantity) {
                user.getCart().getByProductDetailId(productDetailId).setQuantity(quantity);
                userService.save(user);
                //vrati success hlasku
                redirectAttributes.addAttribute("cart_success", true);
            } else {
                redirectAttributes.addAttribute("error_not_enough_stock", true);
                //vrati chybovou hlasku
            }
        } else if (unitsInStock >= quantity) {
            user.getCart().getCartProducts().add(new CartProduct(productDetailRepository.getOne(productDetailId), quantity));
            userService.save(user);
            //vrati success hlasku
            redirectAttributes.addAttribute("cart_success", true);
        } else {
            //vrati chybovou hlasku
            redirectAttributes.addAttribute("error_not_enough_stock", true);
        }
        //vrati se na stranku produktu
        int productId = productDetailRepository.getOne(productDetailId).getProduct().getId();
        return "redirect:/products/detail?id=" + productId;
    }


    /**
     * Removes item from cart by productDetail id
     *
     * @param cartProducDetailtId ProductDetailId in CartProduct
     * @return to cart index
     */
    @RequestMapping("/removeFromCart")
    public String removeFromCart(@RequestParam("cartProductDetailId") int cartProducDetailtId, RedirectAttributes redirectAttributes) {
        user.getCart().getCartProducts().remove(user.getCart().getByProductDetailId(cartProducDetailtId));
        userService.save(user);
        redirectAttributes.addAttribute("remove_success", true);
        return "redirect:/cart/index";
    }
}
