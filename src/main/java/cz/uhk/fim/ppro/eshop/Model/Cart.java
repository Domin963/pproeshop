package cz.uhk.fim.ppro.eshop.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CartProduct> cartProducts;

    public Cart() {
        cartProducts = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<CartProduct> getCartProducts() {
        return cartProducts;
    }

    public void setCartProducts(List<CartProduct> cartProducts) {
        this.cartProducts = cartProducts;
    }

    /**
     * Returns CartProduct from cartProducts list if exists
     *
     * @param id of ProductDetail in CartProduct
     * @return CartProduct or null
     */
    public CartProduct getByProductDetailId(int id) {
        return cartProducts.stream()
                .filter(cartProduct -> id == cartProduct.getProductDetail().getId()).findAny().orElse(null);

    }
}
