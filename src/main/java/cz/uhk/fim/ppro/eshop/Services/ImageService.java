package cz.uhk.fim.ppro.eshop.Services;

import cz.uhk.fim.ppro.eshop.Interfaces.IPhotoStorageRepository;
import cz.uhk.fim.ppro.eshop.Model.PhotoStorage;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class ImageService {
    private IPhotoStorageRepository photoStorageRepository;

    public ImageService(IPhotoStorageRepository photoStorageRepository) {
        this.photoStorageRepository = photoStorageRepository;
    }

    /**
     * Method for saving an image in selected directory
     *
     * @param file MultipartFile to save
     * @param dir  directory, where image should be saved
     * @return String filename, generated UUID
     * @throws IOException
     */
    public String saveImage(MultipartFile file, String dir) throws IOException {
        String filename = UUID.randomUUID().toString().substring(0, 10) + "." + FilenameUtils.getExtension(file.getOriginalFilename());
        Path uploadPath = Paths.get(dir);
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        BufferedImage image = ImageIO.read(file.getInputStream());
        BufferedImage scaled = Scalr.resize(image, 500);
        try {
            Path filePath = uploadPath.resolve(filename);
            ImageIO.write(scaled, FilenameUtils.getExtension(file.getOriginalFilename()), new File(String.valueOf(filePath)));
        } catch (IOException ioException) {
            throw new IOException("Could not save image file " + filename, ioException);
        }
        return filename;
    }

    public PhotoStorage savePhotoStorage(PhotoStorage ps) {
        return photoStorageRepository.save(ps);
    }
}