package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Material;

public interface IMaterialRepository extends ExtRepo<Material, Integer> {
    boolean existsByName(String name);
}
