package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Category;
import cz.uhk.fim.ppro.eshop.Model.Material;
import cz.uhk.fim.ppro.eshop.Model.Product;
import cz.uhk.fim.ppro.eshop.Model.Shape;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IProductRepository extends ExtRepo<Product, Integer> {
    List<Product> getAllByCategory(Category category);
    List<Product> getAllByCategory(Category category, Pageable pageable);
    List<Product> getAllByAvailableMaterials(Material material);
    List<Product> getAllByAvailableShapes(Shape shape);
}
