package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.*;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IProductDetailRepository extends ExtRepo<ProductDetail, Integer> {
    List<ProductDetail> getAllByProduct(Product product);
    List<ProductDetail> getAllByShape(Shape shape);
    List<ProductDetail> getAllByMaterial(Material material);
    List<ProductDetail> getAllByProductCategory(Category category);
}
