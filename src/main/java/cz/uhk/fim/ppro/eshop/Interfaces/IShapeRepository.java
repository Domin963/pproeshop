package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Shape;

public interface IShapeRepository extends ExtRepo<Shape, Integer> {
    public boolean existsByName(String name);
}
