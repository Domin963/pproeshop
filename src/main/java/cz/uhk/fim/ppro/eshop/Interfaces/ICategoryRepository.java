package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Category;

public interface ICategoryRepository extends ExtRepo<Category, Integer> {
    boolean existsByName(String name);

}
