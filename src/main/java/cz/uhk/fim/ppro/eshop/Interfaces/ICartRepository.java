package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Cart;

import java.util.Optional;

public interface ICartRepository extends ExtRepo<Cart, Integer> {
}
