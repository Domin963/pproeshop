package cz.uhk.fim.ppro.eshop.Admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/admin")
public class AdminController {
    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        return "admin/index";
    }
}
