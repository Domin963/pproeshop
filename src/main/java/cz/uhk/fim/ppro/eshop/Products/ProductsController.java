package cz.uhk.fim.ppro.eshop.Products;

import cz.uhk.fim.ppro.eshop.Interfaces.*;
import cz.uhk.fim.ppro.eshop.Model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/products")
public class ProductsController {
    @Autowired
    private ICategoryRepository categoryRepository;
    @Autowired
    private IMaterialRepository materialRepository;
    @Autowired
    private IShapeRepository shapeRepository;
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IProductDetailRepository productDetailRepository;
    @Autowired
    private IReviewRepository reviewRepository;

    @GetMapping(value = {"", "/", "/index"})
    public String index(Model model,
                        @RequestParam("page") Optional<Integer> page,
                        @ModelAttribute("catId") ArrayList<Category> cats,
                        @ModelAttribute("matId") ArrayList<Material> mats,
                        @ModelAttribute("shaId") ArrayList<Shape> shas)
    {
        boolean validation = true; //show slider

        //pagination
        int currentPage = page.orElse(1);
        int pageSize = 9;
        int totalItems;
        int pages;
        Pageable pageable = PageRequest.of(currentPage - 1, pageSize);

        if (!cats.isEmpty() || !mats.isEmpty() || !shas.isEmpty()) {

            List<Product> chkCats = new ArrayList<>();

            if(!cats.isEmpty()){
                for (Category cat : cats) {
                    chkCats.addAll(productRepository.getAllByCategory(categoryRepository.getOne(cat.getId())));
                }
            }else{
                if(!mats.isEmpty()){
                    for (Material mat : mats) {
                        chkCats.addAll(productRepository.getAllByAvailableMaterials(mat));
                    }
                }else{
                    if(!shas.isEmpty()){
                        for (Shape sha : shas) {
                            chkCats.addAll(productRepository.getAllByAvailableShapes(sha));
                        }
                    }
                }
            }

            totalItems = chkCats.size();//productRepository.getAllByCategory(categoryRepository.getOne(catId.get()), pageable).size();
            model.addAttribute("products", chkCats); //productRepository.getAllByCategory(categoryRepository.getOne(catId.get()), pageable));
            validation = false; //turn off slider when specify category
        }
        else {
            totalItems = (int) productRepository.count();
            model.addAttribute("products", productRepository.findAll(pageable).getContent());
        }
        pages = (int) Math.ceil(totalItems / (double) pageSize);

        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("validate", validation);
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("materials", materialRepository.findAll());
        model.addAttribute("shapes", shapeRepository.findAll());

        return "products/index";
    }

    @GetMapping("/detail")
    public String detail(Model model, @RequestParam(name = "id") int prodId) {
        Product product = productRepository.getOne(prodId);
        List<ProductDetail> productDetail = productDetailRepository.getAllByProduct(product);
        List<Review> reviews = reviewRepository.getAllByRewProduct(product);
        BigDecimal score = new BigDecimal(0);
        Double dScore = 0.;
        if(reviews.isEmpty()){}else {
            for(int i = 0;i<reviews.size();i++){
                score = reviews.get(i).getScore().add(score);
            }
            dScore = score.doubleValue();
            dScore = dScore/reviews.size();
        }

        model.addAttribute("score", dScore);
        model.addAttribute("details", productDetail);
        model.addAttribute("product", product);
        model.addAttribute("details", productDetailRepository.getAllByProduct(product));
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("materials", materialRepository.findAll());
        model.addAttribute("shapes", shapeRepository.findAll());
        return "products/detail";
    }

    @GetMapping(value = {"/reviews"})
    public String index(Model model, @RequestParam(name = "id") int id){
        model.addAttribute("reviews", reviewRepository.getAllByRewProduct(productRepository.getOne(id)));
        model.addAttribute("product",productRepository.getOne(id));
        return "products/reviews";
    }
}
