package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Order;

import java.util.List;

public interface IOrderRepository extends ExtRepo<Order, Integer> {
    List<Order> getAllByUserId(int userId);

    List<Order> getAllByUuid(String uuid);
}
