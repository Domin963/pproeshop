package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Order;
import cz.uhk.fim.ppro.eshop.Model.OrderDetail;
import cz.uhk.fim.ppro.eshop.Model.ProductDetail;

import java.util.List;

public interface IOrderDetailRepository extends ExtRepo<OrderDetail, Integer> {
    List<OrderDetail> getAllByProductDetail(ProductDetail productDetail);

    List<OrderDetail> getAllByOrder(Order order);
}
