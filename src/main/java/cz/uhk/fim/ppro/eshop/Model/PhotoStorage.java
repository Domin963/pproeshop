package cz.uhk.fim.ppro.eshop.Model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Blob;

@Entity
@Table(name = "photo_storage")
public class PhotoStorage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private ProductDetail productDetail;
    @NotEmpty
    private String filePath;

    public PhotoStorage() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
