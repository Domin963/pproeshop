package cz.uhk.fim.ppro.eshop.Admin.orders;

import cz.uhk.fim.ppro.eshop.Interfaces.IOrderDetailRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.IOrderRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.IProductDetailRepository;
import cz.uhk.fim.ppro.eshop.Model.Order;
import cz.uhk.fim.ppro.eshop.Model.OrderDetail;
import cz.uhk.fim.ppro.eshop.Model.ProductDetail;
import cz.uhk.fim.ppro.eshop.Model.User;
import cz.uhk.fim.ppro.eshop.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin/orders")
public class AdminOrdersController {
    @Autowired
    private IOrderRepository orderRepository;
    @Autowired
    private IOrderDetailRepository orderDetailRepository;
    UserService userService;
    @Autowired
    IProductDetailRepository productDetailRepository;

    public AdminOrdersController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = {"/", "/index", ""})
    public String index(Model model, @RequestParam("page") Optional<Integer> page,
                        @RequestParam("size") Optional<Integer> size, @RequestParam("searchText") Optional<String> searchText) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = (int) orderRepository.count();
        int pages = (int) Math.ceil(totalItems / (double) pageSize);
        Pageable pageAble = PageRequest.of(currentPage - 1, pageSize);
        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", currentPage);
        //vyhledavani
        if (searchText.isPresent()) {
            List<Order> lstSearchOrders = orderRepository.getAllByUuid(searchText.get());
            List<User> lstSearchUsers = userService.getAllByNameIsContainingOrSurnameIsContaining(searchText.get(), searchText.get());
            for (User u : lstSearchUsers) {
                List<Order> o = orderRepository.getAllByUserId(u.getId());
                if (!o.isEmpty()) {
                    lstSearchOrders.addAll(o);
                }
            }
            if (!lstSearchOrders.isEmpty()) {
                model.addAttribute("orders", lstSearchOrders);
            }
        } else {
            model.addAttribute("orders", orderRepository.findAll(pageAble).getContent());
        }
        return "admin/orders/index";
    }

    @GetMapping("/detail")
    public String detail(@RequestParam("id") int id, Model model) {
        Order order = orderRepository.getOne(id);
        List<OrderDetail> lstOrderDetail = orderDetailRepository.getAllByOrder(order);
        model.addAttribute("lstOrderDetail", lstOrderDetail);
        model.addAttribute("order", order);
        return "admin/orders/detail";
    }

    @RequestMapping("/editOrder")
    public String editOrder(@RequestParam("id") int id, Model model) {
        model.addAttribute("order", orderRepository.getOne(id));
        return "admin/orders/edit";
    }

    @PostMapping("/editOrder")
    public String postEdit(@Valid Order order, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "admin/orders/edit";
        }
        Order o = orderRepository.getOne(order.getId());
        o.setPaid(order.isPaid());
        //bylo poslane, zmeneno na nebylo
        if (o.getShipDate() != null && order.isFulfilled() == false) {
            o.setShipDate(null);
            o.setFulfilled(false);
            //nebylo poslane, zmeneno na poslane
        } else if (order.getShipDate() == null && order.isFulfilled() == true) {
            o.setShipDate(LocalDate.now());
            o.setFulfilled(true);
        }
        o.getAddress().setStreetNameHouseNum(order.getAddress().getStreetNameHouseNum());
        o.getAddress().setCity(order.getAddress().getCity());
        o.getAddress().setPostalCode(order.getAddress().getPostalCode());

        orderRepository.save(o);
        redirectAttributes.addAttribute("update_success", true);

        return "redirect:/admin/orders/";
    }

    /**
     * Method for cancelling order if it wasn't shipped yet, adds quantity back in stock
     *
     * @param id
     * @param redirectAttributes success or error message
     * @return redirect admin/orders
     */
    @RequestMapping("/cancel")
    public String cancelOrder(@RequestParam("id") int id, RedirectAttributes redirectAttributes) {
        Order order = orderRepository.getOne(id);
        if (!order.isFulfilled()) {
            order.setCancelled(true);
            orderRepository.save(order);
            List<ProductDetail> lstProdDetails = new ArrayList<>();
            for (OrderDetail o : order.getOrderDetails()) {
                ProductDetail p = productDetailRepository.getOne(o.getProductDetail().getId());
                p.setUnitsInStock(p.getUnitsInStock() + o.getQuantity());
                lstProdDetails.add(p);
            }
            productDetailRepository.saveAll(lstProdDetails);
            redirectAttributes.addAttribute("order_cancel_success", true);
        } else {
            redirectAttributes.addAttribute("order_cancel_error", true);
        }
        return "redirect:/admin/orders/";
    }
}
