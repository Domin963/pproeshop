package cz.uhk.fim.ppro.eshop.Model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product_details")
public class ProductDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne()
    private Product product;

    @NotNull
    @Min(value = 0)
    private float width;
    @NotNull
    @Min(value = 0)
    private float height;
    @NotNull
    @Min(value = 0)
    private float depth;
    @NotNull
    @Min(value = 0)
    private float weight;
    @ManyToOne
    private Material material;
    @ManyToOne
    private Shape shape;
    @NotNull
    @Min(value = 0)
    private int unitsInStock;
    @NotNull
    @Min(value = 0, message = "Cena nesmí být menší než 0!")
    private BigDecimal unitPrice; //"nice" price with tax
    @NotNull
    private BigDecimal tax;
    @NotNull
    private float taxation;
    @OneToMany(mappedBy = "productDetail")
    private List<PhotoStorage> images;

    public ProductDetail() {
        images = new ArrayList<>();
        taxation=0.21f;
    }

    public ProductDetail(Product product, @NotNull float width, @NotNull float height, @NotNull float depth, @NotNull float weight, @NotNull int unitsInStock, @NotNull BigDecimal unitPrice) {
        taxation = 0.21f;
        images = new ArrayList<>();
        this.product = product;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
        this.unitsInStock = unitsInStock;
        setUnitPrice(unitPrice);
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        tax = unitPrice.subtract(unitPrice.divide(BigDecimal.valueOf(1+taxation), 2, RoundingMode.HALF_EVEN));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public List<PhotoStorage> getImages() {
        return images;
    }

    public void setImages(List<PhotoStorage> images) {
        this.images = images;
    }

    public int getUnitsInStock() {
        return unitsInStock;
    }

    public void setUnitsInStock(int unitsInStock) {
        this.unitsInStock = unitsInStock;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public float getTaxation() {
        return taxation;
    }

    public void setTaxation(float taxation) {
        this.taxation = taxation;
    }
}
