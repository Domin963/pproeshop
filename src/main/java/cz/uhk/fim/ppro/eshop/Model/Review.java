package cz.uhk.fim.ppro.eshop.Model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty
    private String text;

    @NotNull
    @Min(value = 0, message = "Recenze nesmí být menší než 0!")
    @Max(value = 5, message = "Recenze nesmí být větší než 5!")
    private BigDecimal score;

    @ManyToOne
    private Product rewProduct;

    public Review() {
    }

    public Review(int id, @NotEmpty String text, @NotNull @Min(value = 0, message = "Recenze nesmí být menší než 0!") @Max(value = 5, message = "Recenze nesmí být větší než 5!") BigDecimal score, Product product) {
        this.id = id;
        this.text = text;
        this.score = score;
        this.rewProduct = product;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getRewProduct() {
        return rewProduct;
    }

    public void setRewProduct(Product rewProduct) {
        this.rewProduct = rewProduct;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }
}
