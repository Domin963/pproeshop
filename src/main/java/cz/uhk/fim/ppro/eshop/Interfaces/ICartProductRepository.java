package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.CartProduct;

public interface ICartProductRepository extends ExtRepo<CartProduct, Integer> {
}
