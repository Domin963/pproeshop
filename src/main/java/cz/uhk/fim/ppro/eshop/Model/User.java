package cz.uhk.fim.ppro.eshop.Model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne()
    private Role role;

    @ManyToOne()
    @NotNull
    @Valid
    private Address address;
    @OneToMany(mappedBy = "user")
    private List<Order> orders;
    @OneToOne(cascade = {CascadeType.ALL})
    private Cart cart;

    @NotEmpty
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String name;

    @NotEmpty
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String surname;

    @Column(unique = true)
    @NotEmpty
    @Pattern(regexp = "^[A-Za-z0-9]*$", message = "Může obsahovat pouze písmena a čísla")
    private String username;

    @NotEmpty
    @Size(min = 6, message = "Heslo musí mít nejméně 6 znaků")
    private String password;

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "Zadaný text není e-mail!")
    private String email;

    @NotNull
    @Pattern(regexp = "^[+]?[()/0-9. -]{9,}$", message = "Špatně zadané telefonní číslo!")
    private String phoneNumber;

    //pro firmu
    private int ico;
    private String dic;

    @Transient
    private boolean accountNotExpired = true;

    @Transient
    private boolean accountNotLocked = true;

    @Transient
    private boolean credentialsNotExpired = true;

    private boolean enabled;

    public User() {
        orders = new ArrayList<>();
        cart = new Cart();
        enabled = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNotExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNotLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNotExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList(role.getName());
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getIco() {
        return ico;
    }

    public void setIco(int ico) {
        this.ico = ico;
    }

    public String getDic() {
        return dic;
    }

    public void setDic(String dic) {
        this.dic = dic;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public void setAccountNotExpired(boolean accountNotExpired) {
        this.accountNotExpired = accountNotExpired;
    }

    public void setAccountNotLocked(boolean accountNotLocked) {
        this.accountNotLocked = accountNotLocked;
    }

    public void setCredentialsNotExpired(boolean credentialsNotExpired) {
        this.credentialsNotExpired = credentialsNotExpired;
    }
}
