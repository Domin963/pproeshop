package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.PhotoStorage;

public interface IPhotoStorageRepository extends ExtRepo<PhotoStorage, Integer> {
}
