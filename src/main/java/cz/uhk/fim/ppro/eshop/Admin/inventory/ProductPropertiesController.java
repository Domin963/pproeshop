package cz.uhk.fim.ppro.eshop.Admin.inventory;


import cz.uhk.fim.ppro.eshop.Interfaces.ICategoryRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.IMaterialRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.IProductDetailRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.IShapeRepository;
import cz.uhk.fim.ppro.eshop.Model.Category;
import cz.uhk.fim.ppro.eshop.Model.Material;
import cz.uhk.fim.ppro.eshop.Model.Shape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/products/properties")
public class ProductPropertiesController {
    @Autowired
    private IMaterialRepository materialRepository;
    @Autowired
    private IShapeRepository shapeRepository;
    @Autowired
    private IProductDetailRepository productDetailRepository;
    @Autowired
    private ICategoryRepository categoryRepository;

    @GetMapping(value = {"/", "/index", ""})
    public String index(Model model) {
        model.addAttribute("materials", materialRepository.findAll(Sort.by(Sort.Direction.ASC, "name")));
        model.addAttribute("shapes", shapeRepository.findAll(Sort.by(Sort.Direction.ASC, "name")));
        model.addAttribute("categories", categoryRepository.findAll(Sort.by(Sort.Direction.ASC, "name")));

        return "admin/products/properties/index";
    }

    @GetMapping("/createMaterial")
    public String createMaterial(Model model, @RequestParam("id")int id) {
        if(id==0) {
            model.addAttribute("material", new Material());
        } else {
            model.addAttribute("material", materialRepository.getOne(id));
        }

        return "admin/products/properties/createMaterial";
    }

    @GetMapping("/createShape")
    public String createShape(Model model, @RequestParam("id")int id) {
        if(id==0) {
            model.addAttribute("shape", new Shape());
        } else {
            model.addAttribute("shape", shapeRepository.getOne(id));
        }
        return "admin/products/properties/createShape";
    }
    @GetMapping("/createCategory")
    public String createCategory(Model model, @RequestParam("id")int id) {
        if(id==0) {
            model.addAttribute("category", new Category());
        } else {
            model.addAttribute("category", categoryRepository.getOne(id));
        }
        return "admin/products/properties/createCategory";
    }


    @PostMapping("/createMaterial")
    public String createMaterial(@Valid Material material, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin/products/properties/createMaterial";
        }
        if (materialRepository.existsByName(material.getName())) { //material jiz existuje
            return "admin/products/properties/createMaterial";
        }
        materialRepository.save(material);
        return "redirect:/admin/products/properties/index";
    }

    @PostMapping("/createShape")
    public String createShape(@Valid Shape shape, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin/products/properties/createShape";
        }
        if (shapeRepository.existsByName(shape.getName())) { //tvar jiz existuje
            return "admin/products/properties/createShape";
        }
        shapeRepository.save(shape);
        return "redirect:/admin/products/properties/index";
    }
    @PostMapping("/createCategory")
    public String createShape(@Valid Category category, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin/products/properties/createCategory";
        }
        if (categoryRepository.existsByName(category.getName())) { //kategorie jiz existuje
            return "admin/products/properties/createCategory";
        }
        categoryRepository.save(category);
        return "redirect:/admin/products/properties/index";
    }

    @GetMapping("/deleteMaterial")
    public String deleteMaterial(@RequestParam("id")int id) {
        if(productDetailRepository.getAllByMaterial(materialRepository.getOne(id)).isEmpty()) {
            materialRepository.deleteById(id);
        }
        return "redirect:/admin/products/properties/index";
    }
    @GetMapping("/deleteShape")
    public String deleteShape(@RequestParam("id")int id) {
        if(productDetailRepository.getAllByShape(shapeRepository.getOne(id)).isEmpty()) {
            shapeRepository.deleteById(id);
        }
        return "redirect:/admin/products/properties/index";
    }
    @GetMapping("/deleteCategory")
    public String deleteCategory(@RequestParam("id")int id) {
        if(productDetailRepository.getAllByProductCategory(categoryRepository.getOne(id)).isEmpty()) {
            categoryRepository.deleteById(id);
        }
        return "redirect:/admin/products/properties/index";
    }
}
