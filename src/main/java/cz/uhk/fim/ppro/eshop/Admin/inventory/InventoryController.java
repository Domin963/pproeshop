package cz.uhk.fim.ppro.eshop.Admin.inventory;

import cz.uhk.fim.ppro.eshop.Interfaces.*;
import cz.uhk.fim.ppro.eshop.Model.*;
import cz.uhk.fim.ppro.eshop.Services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin/products")
public class InventoryController {
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IProductDetailRepository productDetailRepository;
    @Autowired
    private IOrderDetailRepository orderDetailRepository;
    @Autowired
    private IMaterialRepository materialRepository;
    @Autowired
    private IShapeRepository shapeRepository;
    @Autowired
    private ICategoryRepository categoryRepository;
    private ImageService imageService;
    private final String fulldir = "product_images/";
    private final String dir = "../product_images/";

    public InventoryController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping(value = {"/", "/index", ""})
    public String index(Model model, @RequestParam("page") Optional<Integer> page,
                        @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = (int) productRepository.count();
        int pages = (int) Math.ceil(totalItems / (double) pageSize);
        Pageable pageAble = PageRequest.of(currentPage - 1, pageSize);
        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", currentPage);


        List<Product> products = productRepository.findAll(pageAble).getContent();
        model.addAttribute("products", products);
        return "admin/products/index";
    }

    /**
     * Method for showing all ProductDetails for selected Product
     *
     * @param id    Product id
     * @param model
     * @return Admin/products/detail page
     */
    @GetMapping("/detail")
    public String detail(@RequestParam("id") int id, Model model) {
        Product product = productRepository.getOne(id);
        List<ProductDetail> productDetail = productDetailRepository.getAllByProduct(product);
        model.addAttribute("productDetail", productDetail);
        model.addAttribute("product", product);
        return "admin/products/detail";
    }

    @GetMapping("/createProduct")
    public String createProduct(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("detail", new ProductDetail());
        model.addAttribute("materials", materialRepository.findAll());
        model.addAttribute("shapes", shapeRepository.findAll());
        model.addAttribute("categories", categoryRepository.findAll());
        return "admin/products/createProduct";
    }

    /**
     * @param model
     * @param id    if id==0 create, else edit
     * @return
     */
    @GetMapping("/createDetail")
    public String createDetail(Model model, @RequestParam("id") int id, @RequestParam("prodId") int prodId) {
        if (id == 0) {
            ProductDetail detail = new ProductDetail();
            detail.setProduct(productRepository.getOne(prodId));
            model.addAttribute("detail", detail);
        } else {
            model.addAttribute("detail", productDetailRepository.getOne(id));
        }
        model.addAttribute("materials", materialRepository.findAll());
        model.addAttribute("shapes", shapeRepository.findAll());
        return "admin/products/createDetail";
    }

    @GetMapping("/editProduct")
    public String editProduct(Model model, @RequestParam("id") int id) {
        model.addAttribute("product", productRepository.getOne(id));
        model.addAttribute("categories", categoryRepository.findAll());
        return "admin/products/editProduct";
    }

    /**
     * Creates Product and one associated ProductDetail
     *
     * @param product
     * @param bindingResult
     * @param detail
     * @param materialId
     * @param shapeId
     * @return redirect to ProductDetail page of Product
     */
    @PostMapping("/createProduct")
    public String create(@Valid Product product, BindingResult bindingResult, ProductDetail detail,
                         @RequestParam("materialId") int materialId, @RequestParam("shapeId") int shapeId, @RequestParam("categoryId") int categoryId, @RequestParam("image") Optional<MultipartFile> file) throws IOException {
        detail.setMaterial(materialRepository.getOne(materialId));
        detail.setShape(shapeRepository.getOne(shapeId));

        if (bindingResult.hasErrors()) {
            return "admin/products/createProduct";
        }
        product.getAvailableMaterials().add(detail.getMaterial());
        product.getAvailableShapes().add(detail.getShape());
        product.setCategory(categoryRepository.getOne(categoryId));
        product = productRepository.save(product);
        detail.setProduct(product);
        detail = productDetailRepository.save(detail);
        //ulozeni fotky
        if (!file.get().isEmpty()) {
            String imgName = imageService.saveImage(file.get(), fulldir);
            PhotoStorage p = new PhotoStorage();
            p.setFilePath(dir + imgName);
            p.setProductDetail(detail);
            p = imageService.savePhotoStorage(p);
            detail.getImages().add(p);
            product.setPhoto(p.getFilePath());
            productRepository.save(product);
            productDetailRepository.save(detail);
        }
        return "redirect:/admin/products/detail?id=" + product.getId();
    }

    @PostMapping("/editProduct")
    public String editProduct(@Valid Product product, BindingResult bindingResult, @RequestParam("categoryId") int categoryId) {
        if (bindingResult.hasErrors()) {
            return "admin/products/editProduct";
        }
        product.setCategory(categoryRepository.getOne(categoryId));
        productRepository.save(product);
        return "redirect:/admin/products/index";
    }

    @PostMapping("/createDetail")
    public String createDetail(@RequestParam("productId") int productId, @Valid ProductDetail detail, BindingResult bindingResult,
                               @RequestParam("materialId") int materialId, @RequestParam("shapeId") int shapeId, @RequestParam("image") Optional<MultipartFile> file) throws IOException {
        Product product = productRepository.getOne(productId);
        if (bindingResult.hasErrors()) {
            return "redirect:/admin/products/createDetail?id=" + detail.getId() + "&prodId=" + productId;
        }
        detail.setProduct(product);
        Shape shape = shapeRepository.getOne(shapeId);
        Material material = materialRepository.getOne(materialId);
        detail.setMaterial(material);
        detail.setShape(shape);
        productDetailRepository.save(detail);
        if (!file.get().isEmpty()) {
            //ulozeni fotky
            String imgName = imageService.saveImage(file.get(), fulldir);
            PhotoStorage p = new PhotoStorage();
            p.setFilePath(dir + imgName);
            p.setProductDetail(detail);
            p = imageService.savePhotoStorage(p);
            detail.getImages().add(p);
            productDetailRepository.save(detail);
        }
        List<Material> materials = new ArrayList<>();
        List<Shape> shapes = new ArrayList<>();
        //pri zmene shape nebo materialu zustava stary v seznamu, je treba aktualizovat seznamy
        List<ProductDetail> products = productDetailRepository.getAllByProduct(product);
        for (ProductDetail p : products) {
            if (!materials.contains(p.getMaterial())) {
                materials.add(p.getMaterial());
            }
            if (!shapes.contains(p.getShape())) {
                shapes.add(p.getShape());
            }
        }
        product.setAvailableMaterials(materials);
        product.setAvailableShapes(shapes);
        productRepository.save(product);
        return "redirect:/admin/products/detail?id=" + productId;
    }

    /**
     * Removes Product and all asociated ProductDetails IF there is no OrderDetail associated with this Product or ProductDetail
     * else set UnitsInStock to 0
     *
     * @param id
     * @return
     */
    @GetMapping("/delete")
    public String delete(@RequestParam("id") int id, RedirectAttributes redirectAttributes) {
        List<ProductDetail> allProductDetails = productDetailRepository.getAllByProduct(productRepository.getOne(id));
        boolean isUsed = false;
        for (ProductDetail x : allProductDetails) {
            //Order exists, no deletion occurs, unitsInStock to zero
            if (!orderDetailRepository.getAllByProductDetail(x).isEmpty()) {
                isUsed = true;
            }
        }
        if (isUsed) {
            for (ProductDetail x : allProductDetails) {
                x.setUnitsInStock(0);
            }
            productDetailRepository.saveAll(allProductDetails);
            redirectAttributes.addAttribute("delete_zero", true);
            return "redirect:/admin/products/";
        }
        productDetailRepository.deleteAll(allProductDetails);
        productRepository.deleteById(id);
        redirectAttributes.addAttribute("delete_success", true);
        return "redirect:/admin/products/";
    }

    /**
     * Removes ProductDetail if there is no OrderDetail associated with this ProductDetail else set unitsInStock to zero
     *
     * @param id ProductDetail id
     * @return redirect to Product details
     */
    @GetMapping("/deleteDetail")
    public String deleteDetail(@RequestParam("id") int id, RedirectAttributes redirectAttributes) {
        int productId = productDetailRepository.getOne(id).getProduct().getId();
        if (orderDetailRepository.getAllByProductDetail(productDetailRepository.getOne(id)).isEmpty()) {
            productDetailRepository.deleteById(id);
            redirectAttributes.addAttribute("delete_success", true);
        } else {
            ProductDetail p = productDetailRepository.getOne(id);
            p.setUnitsInStock(0);
            productDetailRepository.save(p);
            redirectAttributes.addAttribute("delete_zero", true);
        }
        return "redirect:/admin/products/detail?id=" + productId;
    }
}
