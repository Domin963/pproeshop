package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.User;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends ExtRepo<User, Integer> {
    Optional<User> getByUsername(String userName);

    List<User> getAllBySurnameIsContainingOrUsernameIsContaining(String surname, String username);

    List<User> getAllByNameIsContainingOrSurnameIsContaining(String name, String surname);
}
