package cz.uhk.fim.ppro.eshop.Model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "adresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty
    @Pattern(regexp = "^[\\p{L} .'-]+[0-9]+$", message = "Může obsahovat pouze písmena a čísla, musí obsahovat číslo popisné")
    private String streetNameHouseNum;

    @NotEmpty
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String city;

    @NotNull
    @Pattern(regexp = "\\d{3} ?\\d{2}", message = "Špatně zadané PSČ")
    private String postalCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreetNameHouseNum() {
        return streetNameHouseNum;
    }

    public void setStreetNameHouseNum(String streetNameHouseNum) {
        this.streetNameHouseNum = streetNameHouseNum;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Address() {
    }
}
