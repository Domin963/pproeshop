package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.Address;

public interface IAddressRepository extends ExtRepo<Address, Integer> {
}
