package cz.uhk.fim.ppro.eshop.Model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    @ManyToMany
    private List<Material> availableMaterials;
    @ManyToMany
    private List<Shape> availableShapes;
    @ManyToOne
    private Category category;
    private String photo;

    @OneToMany(mappedBy = "rewProduct")
    private List<Review> reviews;

    public Product() {
        availableMaterials = new ArrayList<>();
        availableShapes = new ArrayList<>();
        reviews = new ArrayList<>();
        photo = "";
    }

    public Product(@NotEmpty String name, @NotEmpty String description) {
        this.name = name;
        this.description = description;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public List<Material> getAvailableMaterials() {
        return availableMaterials;
    }

    public void setAvailableMaterials(List<Material> availableMaterials) {
        this.availableMaterials = availableMaterials;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Shape> getAvailableShapes() {
        return availableShapes;
    }

    public void setAvailableShapes(List<Shape> availableShapes) {
        this.availableShapes = availableShapes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

