package cz.uhk.fim.ppro.eshop.Interfaces;

import cz.uhk.fim.ppro.eshop.Model.*;

import java.util.List;
import java.util.Optional;

public interface IReviewRepository extends ExtRepo<Review, Integer> {
    List<Review> getAllByRewProduct(Product product);
}
