package cz.uhk.fim.ppro.eshop.Services;

import cz.uhk.fim.ppro.eshop.Interfaces.IUserRepository;
import cz.uhk.fim.ppro.eshop.Model.User;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userRepository.getByUsername(userDetails.getUsername());
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public long count() {
        return userRepository.count();
    }

    public List<User> getAllByNameIsContainingOrSurnameIsContaining(String s, String s1) {
        return userRepository.getAllByNameIsContainingOrSurnameIsContaining(s, s1);
    }

    public List<User> getAllBySurnameIsContainingOrUsernameIsContaining(String s, String s1) {
        return userRepository.getAllBySurnameIsContainingOrUsernameIsContaining(s, s1);
    }

    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    public User getOne(int id) {
        return userRepository.getOne(id);
    }

    public Optional<User> getByUsername(String username) {
        return userRepository.getByUsername(username);
    }
}
