package cz.uhk.fim.ppro.eshop;


import cz.uhk.fim.ppro.eshop.Interfaces.IAddressRepository;
import cz.uhk.fim.ppro.eshop.Interfaces.IRoleRepository;
import cz.uhk.fim.ppro.eshop.Model.Address;
import cz.uhk.fim.ppro.eshop.Model.Role;
import cz.uhk.fim.ppro.eshop.Model.User;
import cz.uhk.fim.ppro.eshop.Services.UserService;
import cz.uhk.fim.ppro.eshop.index.HomeController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest
@WebAppConfiguration
public class HomeControllerTest {

    @Mock
    UserService userService;
    @Mock
    IRoleRepository roleRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    IAddressRepository addressRepository;
    @InjectMocks
    HomeController testedController;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        // this must be called for the @Mock annotations above to be processed
        // and for the mock service to be injected into the controller under
        // test.
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(testedController, "roleRepository", roleRepository);
        ReflectionTestUtils.setField(testedController, "addressRepository", addressRepository);
        ReflectionTestUtils.setField(testedController, "passwordEncoder", passwordEncoder);
        this.mockMvc = MockMvcBuilders.standaloneSetup(testedController).build();
        Role role = new Role();
        role.setName("user");
        when(roleRepository.getByName("user")).thenReturn(Optional.of(role));
        when(addressRepository.save(Mockito.any(Address.class))).thenReturn(null);
        when(userService.save(Mockito.any(User.class))).thenReturn(null);
    }

    @Test
    public void createUserFailsNotEnoughData() throws Exception {
        mockMvc.perform(post("/postRegister").param("name", "user"))
                .andExpect(model().attributeHasFieldErrors("user")).andExpect(view().name("register"));
    }

    @Test
    public void createUserSuccess() throws Exception {
        Address addr1 = new Address();
        addr1.setCity("Jihlava");
        addr1.setPostalCode("58601");
        addr1.setStreetNameHouseNum("Jihlavská 12");

        User user = new User();
        user.setName("Test");
        user.setSurname("Test");
        user.setEmail("test@mail.com");
        user.setPhoneNumber("123456789");
        user.setUsername("test");
        user.setPassword("testtesttest");
        user.setAddress(addr1);
        mockMvc.perform(post("/postRegister").flashAttr("user", user)).andExpect(view().name("login"));
    }
}
