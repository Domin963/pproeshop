package cz.uhk.fim.ppro.eshop;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Pattern;

public class JUnitUserRegex {
    String regPhone = "^[+]?[()/0-9. -]{9,}$";
    String regUsername = "^[A-Za-z0-9]*$";

    @Test
    public void checkRegexUsernameTrue(){
        Assert.assertTrue(Pattern.matches(regUsername,
                "franta1"));
    }

    @Test
    public void checkRegexUsernameFalse(){
        Assert.assertFalse(Pattern.matches(regUsername,
                "xD.ahoj"));
    }

    @Test
    public void checkRegexPhoneTrue(){
        Assert.assertTrue(Pattern.matches(regPhone,
                "+123456789"));
    }
    @Test
    public void checkRegexPhoneFalse(){
        Assert.assertFalse(Pattern.matches(regPhone,
                "12345678"));
    }
    @Test
    public void checkRegexPhoneFalse2(){
        Assert.assertFalse(Pattern.matches(regPhone,
                "1234abc891"));
    }
}
